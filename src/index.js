import React from 'react';
import {View, StyleSheet} from 'react-native';
import FaceRecognition from '@features/FaceRecognition2';
import OneSignal from 'react-native-onesignal';

const useStyles = () => {
  return StyleSheet.create({
    container: {
      flex: 1,
    },
  });
};

const App = () => {
  const Styles = useStyles();

  React.useEffect(() => {
    OneSignal.setAppId('e1f160bd-c8d7-41f2-ba16-ee3838f018b1');

    OneSignal.setNotificationWillShowInForegroundHandler(
      notificationReceivedEvent => {
        // DO SOMETHING WITH THE NOTIFICATION
      },
    );

    OneSignal.setNotificationOpenedHandler(notification => {
      // DO SOMETHING WITH THE NOTIFICATION
    });
  }, []);

  return (
    <View style={Styles.container}>
      <FaceRecognition />
    </View>
  );
};

export default App;
