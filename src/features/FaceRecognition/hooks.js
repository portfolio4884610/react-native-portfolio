import React from 'react';
import {NativeEventEmitter} from 'react-native';
import FaceSDK, {
  Enum,
  FaceCaptureResponse,
  MatchFacesImage,
  MatchFacesRequest,
  MatchFacesResponse,
} from '@regulaforensics/react-native-face-api';

let image1 = new MatchFacesImage();
let image2 = new MatchFacesImage();

const API_KEY = 'ZmUxYTI3NDItODFkYS00NTg5LWI0MTEtYzJkZTAyNzRjNTYx';
const ONESIGNAL_APP_ID = 'e1f160bd-c8d7-41f2-ba16-ee3838f018b1';
const BASE_URL = 'https://onesignal.com/api/v1/notifications';

export const useFaceRecognitionHooks = () => {
  const [liveness, setLiveness] = React.useState('nil');
  const [similarity, setSimilarity] = React.useState('nil');
  const [currentDetectedFace, setCurrentDetectedFace] = React.useState('nil');
  const [matched, setMatched] = React.useState(null);
  const [isLoading, setIsLoading] = React.useState(false);

  const sendPushNotification = async () => {
    try {
      const body = {
        app_id: ONESIGNAL_APP_ID,
        included_segments: ['Subscribed Users'],
        contents: {
          en: 'Face successfully recognized. Welcome User!',
        },
      };

      await fetch(BASE_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${API_KEY}`,
        },
        body: JSON.stringify(body),
      });
    } catch (error) {
      console.log(error);
      throw error;
    }
  };

  const setFaceAuth = () => {
    FaceSDK.presentFaceCaptureActivity(
      result => {
        const BASE64 = FaceCaptureResponse.fromJson(JSON.parse(result)).image
          .bitmap;
        const TYPE = Enum.ImageType.LIVE;

        image1.bitmap = BASE64;
        image1.imageType = TYPE;

        setCurrentDetectedFace('data:image/png;base64,' + BASE64);
      },
      e => {
        alert('something went wrong');
      },
    );
  };

  const validateFaceAuth = payload => {
    if (isLoading) return;

    setIsLoading(true);

    FaceSDK.presentFaceCaptureActivity(
      result => {
        const BASE64 = FaceCaptureResponse.fromJson(JSON.parse(result)).image
          .bitmap;
        const TYPE = Enum.ImageType.LIVE;

        image2.bitmap = BASE64;
        image2.imageType = TYPE;

        let request = new MatchFacesRequest();
        request.images = [image1, image2];

        FaceSDK.matchFaces(
          JSON.stringify(request),
          async r => {
            response = MatchFacesResponse.fromJson(JSON.parse(r));

            const {similarity, score} = response.results[0];
            const testMatch = similarity - score;
            const THRESHOLD = 0.75;

            if (testMatch > THRESHOLD) {
              setMatched(true);
              setIsLoading(false);
              await sendPushNotification();
            } else {
              setMatched(false);
              setIsLoading(false);
            }
          },
          e => {
            setIsLoading(false);
            console.log(e);
          },
        );
      },
      e => {
        setIsLoading(false);
        alert('something went wrong');
      },
    );
  };

  return {
    liveness,
    setLiveness,
    similarity,
    setSimilarity,
    currentDetectedFace,
    setCurrentDetectedFace,
    setFaceAuth,
    validateFaceAuth,
    matched,
    setMatched,
    isLoading,
    sendPushNotification,
  };
};
