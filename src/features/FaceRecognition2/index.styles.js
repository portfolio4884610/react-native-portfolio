import {StyleSheet} from 'react-native';

const useStyles = () => {
  return StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    auth_btn_face_detect: {
      padding: 15,
      borderRadius: 5,
      backgroundColor: '#cc4b00',
      marginTop: 10,
    },
    auth_btn_login: {
      padding: 15,
      borderRadius: 5,
      backgroundColor: '#3399ff',
      marginTop: 10,
      justifyContent: 'center',
      alignItems: 'center',
    },
    auth_btn_tryagain: {
      padding: 15,
      borderRadius: 5,
      backgroundColor: '#59ffa1',
      marginTop: 10,
      justifyContent: 'center',
      alignItems: 'center',
    },
    logout_btn: {
      padding: 15,
      borderRadius: 5,
      backgroundColor: 'red',
      marginTop: 10,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });
};

export default useStyles;
