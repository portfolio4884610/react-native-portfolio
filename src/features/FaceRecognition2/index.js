import React from 'react';
import {
  View,
  Text,
  NativeEventEmitter,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import useStyles from './index.styles';
import {useFaceRecognitionHooks} from './hooks';

import FaceSDK, {
  Enum,
  FaceCaptureResponse,
  LivenessResponse,
  MatchFacesResponse,
  MatchFacesRequest,
  MatchFacesImage,
  MatchFacesSimilarityThresholdSplit,
  RNFaceApi,
} from '@regulaforensics/react-native-face-api';

const FaceRecognition = () => {
  const Styles = useStyles();
  const {
    liveness,
    setLiveness,
    similarity,
    setSimilarity,
    currentDetectedFace,
    setCurrentDetectedFace,
    setFaceAuth,
    validateFaceAuth,
    matched,
    setMatched,
    isLoading,
  } = useFaceRecognitionHooks();

  return (
    <View style={Styles.container}>
      <Text style={{fontWeight: 'bold', fontSize: 20}}>
        FACE RECOGNITION AUTH
      </Text>

      <View style={{height: 20}} />

      {currentDetectedFace === 'nil' ? (
        <TouchableOpacity
          style={Styles.auth_btn_face_detect}
          onPress={setFaceAuth}>
          <Text style={{color: '#fff'}}>Setup a Face Detection Auth</Text>
        </TouchableOpacity>
      ) : matched === true ? (
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Text>SUCCESSFULLY LOGGED IN</Text>
          <Text>Notification will arrive in a few seconds.</Text>
          <TouchableOpacity
            style={Styles.logout_btn}
            onPress={() => {
              setMatched(null);
              setLiveness('nil');
              setSimilarity('nil');
            }}>
            <Text style={{color: '#fff'}}>Logout</Text>
          </TouchableOpacity>
        </View>
      ) : matched === false ? (
        <>
          <Text>FACE NOT MATCHED</Text>
          <TouchableOpacity
            style={Styles.auth_btn_tryagain}
            onPress={() => setMatched(null)}>
            <Text>Try Again</Text>
          </TouchableOpacity>
        </>
      ) : (
        <View>
          {isLoading ? (
            <Text
              style={{
                textAlign: 'center',
              }}>{`This may take a moment depending\non your internet connection`}</Text>
          ) : null}
          <TouchableOpacity
            style={Styles.auth_btn_login}
            onPress={() => validateFaceAuth(currentDetectedFace)}>
            <Text style={{color: '#fff'}}>
              {isLoading ? (
                <ActivityIndicator size="large" color="#fff" />
              ) : (
                'Login via Face Detection'
              )}
            </Text>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

export default FaceRecognition;
