import React from 'react';
import {NativeEventEmitter} from 'react-native';
import FaceSDK, {
  Enum,
  FaceCaptureResponse,
  MatchFacesImage,
  MatchFacesRequest,
  MatchFacesResponse,
  MatchFacesSimilarityThresholdSplit,
  ImageType,
} from '@regulaforensics/react-native-face-api';

let image1 = new MatchFacesImage();
let image2 = new MatchFacesImage();

const API_KEY = 'ZmUxYTI3NDItODFkYS00NTg5LWI0MTEtYzJkZTAyNzRjNTYx';
const ONESIGNAL_APP_ID = 'e1f160bd-c8d7-41f2-ba16-ee3838f018b1';
const BASE_URL = 'https://onesignal.com/api/v1/notifications';

export const useFaceRecognitionHooks = () => {
  const [liveness, setLiveness] = React.useState('nil');
  const [similarity, setSimilarity] = React.useState('nil');
  const [currentDetectedFace, setCurrentDetectedFace] = React.useState('nil');
  const [matched, setMatched] = React.useState(null);
  const [isLoading, setIsLoading] = React.useState(false);

  React.useEffect(() => {
    FaceSDK.init(
      json => {},
      e => {},
    );
  }, []);

  const sendPushNotification = async () => {
    try {
      const body = {
        app_id: ONESIGNAL_APP_ID,
        included_segments: ['Subscribed Users'],
        contents: {
          en: 'Face successfully recognized. Welcome User!',
        },
      };

      await fetch(BASE_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${API_KEY}`,
        },
        body: JSON.stringify(body),
      });
    } catch (error) {
      throw error;
    }
  };

  const setFaceAuth = () => {
    FaceSDK.presentFaceCaptureActivity(
      result => {
        const BASE64 = FaceCaptureResponse.fromJson(JSON.parse(result)).image
          .bitmap;
        const TYPE = ImageType.PRINTED;

        image1.bitmap = BASE64;
        image1.imageType = TYPE;

        setCurrentDetectedFace('data:image/bmp;base64,' + BASE64);
      },
      e => {
        alert('something went wrong');
      },
    );
  };

  const validateFaceAuth = payload => {
    if (isLoading) return;

    setIsLoading(true);

    FaceSDK.presentFaceCaptureActivity(
      result => {
        const BASE64 = FaceCaptureResponse.fromJson(JSON.parse(result)).image
          .bitmap;
        const TYPE = ImageType.PRINTED;

        image2.bitmap = BASE64;
        image2.imageType = TYPE;

        const request = new MatchFacesRequest();
        request.images = [image1, image2];

        FaceSDK.matchFaces(
          JSON.stringify(request),
          matchFacesResponse => {
            response = MatchFacesResponse.fromJson(
              JSON.parse(matchFacesResponse),
            );

            if (response.exception) {
              setIsLoading(false);
              alert('something went wrong, api call failed');
              return;
            }

            FaceSDK.matchFacesSimilarityThresholdSplit(
              JSON.stringify(response.results),
              0.75,
              async splitResponse => {
                const split = MatchFacesSimilarityThresholdSplit.fromJson(
                  JSON.parse(splitResponse),
                );
                const {matchedFaces} = split;

                if (matchedFaces.length > 0) {
                  const matchData = matchedFaces[0];
                  const {similarity, score} = matchData;
                  const testMatch = similarity - score;
                  const THRESHOLD = 0.75;

                  setMatched(true);
                  setIsLoading(false);
                  await sendPushNotification();
                } else {
                  setMatched(false);
                  setIsLoading(false);
                }
              },
              e => {},
            );

            // const testMatch = similarity - score;
            // const THRESHOLD = 0.75;

            // if (testMatch > THRESHOLD) {
            //   setMatched(true);
            //   setIsLoading(false);
            //   await sendPushNotification();
            // } else {
            //   setMatched(false);
            //   setIsLoading(false);
            // }
          },
          e => {
            setIsLoading(false);
          },
        );
      },
      e => {
        setIsLoading(false);
        alert('something went wrong');
      },
    );
  };

  return {
    liveness,
    setLiveness,
    similarity,
    setSimilarity,
    currentDetectedFace,
    setCurrentDetectedFace,
    setFaceAuth,
    validateFaceAuth,
    matched,
    setMatched,
    isLoading,
    sendPushNotification,
  };
};
